class PostsController < ApplicationController
  # action can with posts can only be performed by admin
  before_action :logged_in_admin, only: [:create, :destroy, :edit, :update]
# post creation and failed save
  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:success] = "Pavyko įkelti"
      redirect_to gallery_url
    else
      flash[:danger] = "Nepavyko sukurti"
      redirect_to gallery_url
    end
  end

  # post show
  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      flash[:success] = "Pataisyti pavyko!"
      redirect_to gallery_url
    else
      flash[:danger] = "Pataisyti nepavyko"
      rener 'edit'
    end
  end

# destroying post
  def destroy
    Post.find(params[:id]).destroy
    flash[:success] = "Ištrintas"
    redirect_to gallery_url
  end


  private

# checking if you are logged in
  def logged_in_admin
    unless logged_in?
      flash[:danger] = "Prisijunkite"
      redirect_to root_url
    end
  end
end
