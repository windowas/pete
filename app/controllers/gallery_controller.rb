class GalleryController < ApplicationController

  def pete
    @posts = Post.all
    @post = Post.new
    # pulling correct data
    @post_feed = @posts.where(purpose: "pete").paginate(page: params[:page], :per_page => 12)
    end

  def kasp
    @posts = Post.all
    @post = Post.new
    @post_feed = @posts.where(purpose: "kasp").paginate(page: params[:page], :per_page => 12)
  end

  def med
    @posts = Post.all
    @post = Post.new
    @post_feed = @posts.where(purpose: "med").paginate(page: params[:page], :per_page => 12)
  end
end
