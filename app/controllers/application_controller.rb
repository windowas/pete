class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  private
#  safe parameter
# allow nested params as array
  def post_params
    params.require(:post).permit(:content, :price, :purpose, :picture)
  end
end
