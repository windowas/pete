class PagesController < ApplicationController

  def home
  end

  def gallery
    @posts = Post.all
    @post = Post.new
    @post_feed = @posts.paginate(page: params[:page], :per_page => 12)
  end

  def contact
  end
end
