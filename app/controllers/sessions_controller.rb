class SessionsController < ApplicationController
  def new
  end

    # sukurus admin per rails console, prisijungimo funkcija
  def create
    user = Admin.first
    if user.nil?
      flash.now[:danger] = "Nesukurtas administratorius"
      render 'new'
    elsif user.authenticate(params[:session][:password])
      log_in user
      redirect_to gallery_url
    else
      flash.now[:danger] = "Blogas slaptažodis"
      render 'new'
    end
  end

# atsijungimo funkcija
  def destroy
    log_out
    redirect_to root_url
  end
end
