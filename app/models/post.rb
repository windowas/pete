class Post < ApplicationRecord
  # pirmas postas naujausias
  default_scope -> { order(created_at: :desc) }
  validates :picture, presence: true
  mount_uploader :picture, PictureUploader

  def next
     Post.where("id < ?", id).first
  end

  def previous
     Post.where("id > ?", id).order(id: :desc).last
  end
end
