class CreateAdmins < ActiveRecord::Migration[5.1]
  def change
    create_table :admins do |t|
      t.string :password_digest
      t.string :remember_digest
      t.boolean :admin

      t.timestamps
    end
  end
end
