class AddPictureToPosts < ActiveRecord::Migration[5.1]
  def change
    # add images column as array
    add_column :posts, :picture, :string
  end
end
