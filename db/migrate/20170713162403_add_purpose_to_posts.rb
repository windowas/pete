class AddPurposeToPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :purpose, :string
  end
end
