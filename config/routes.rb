Rails.application.routes.draw do
  root  'pages#home'
  get   '/admin',                to: 'sessions#new'
  post  '/admin',                to: 'sessions#create'
  get   '/logout',               to: 'sessions#destroy'
  get   '/contact',              to: 'pages#contact'
  get   '/home',                 to: 'pages#home'
  get   '/gallery',              to: 'pages#gallery'
  get   '/peteliskes',           to: 'gallery#pete'
  get   '/kaspineliai_plaukams', to: 'gallery#kasp'
  get   '/medziagos',            to: 'gallery#med'

  resources :posts 
  resources :admins
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
